function Point(x, y, z) {
  this.x = x || 0;
  this.y = y || 0;
  this.z = z || 0;
}

Point.prototype = {
  equals: function(p) {
    return this.x == p.x && this.y == p.y && this.z == p.z;
  },
  subtractPoint: function(p) {
    return new Vector(p.x - this.x, p.y - this.y, p.z - this.z).negative();
  },
  addVector: function(v) {
    return new Point(this.x + v.x, this.y + v.y, this.z + v.z);
  },
  toArray: function(n) {
    return [this.x, this.y, this.z].slice(0, n || 3);
  },
  clone: function() {
    return new Point(this.x, this.y, this.z);
  },
  init: function(x, y, z) {
    this.x = x; this.y = y; this.z = z;
    return this;
  }
};

Point.fromArray = function(a) {
  return new Point(a[0], a[1], a[2]);
};