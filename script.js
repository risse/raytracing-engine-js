const DISPLAY_WIDTH = 768;
const DISPLAY_HEIGHT = 432;

const display = document.getElementById('display');

const ctx = display.getContext('2d');

drawFrame();

var point1 = new Point(1,2,1);
var point2 = new Point(0,4,4);
var vector1 = new Vector(2,0,0);

var vector2;

console.log(point1); //should display (1,2,1)
console.log(point2); //should display (0,4,4)

vector2 = point1.subtractPoint(point2);

console.log(vector2);

vector1 = vector1.add(vector2);

point1.addVector(vector1);
console.log(point1); //should display (4,0,-2)

point2.subtractVectorFromPoint(vector2);
point2.drawPoint(); //should display (-1,6,7)

function drawFrame() {

  var imageData = ctx.getImageData(0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT);

  for (var i = 0; i < imageData.data.length; i += 4) {
    imageData.data[i] = Math.floor(Math.random(0)*255);
    imageData.data[i + 1] = Math.floor(Math.random(0)*255);
    imageData.data[i + 2] = Math.floor(Math.random(0)*255);
    imageData.data[i + 3] = 255;
  }

  ctx.putImageData(imageData, 0, 0);

}